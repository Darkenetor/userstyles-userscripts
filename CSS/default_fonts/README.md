- Ignores icon fonts.

- Might cause slight slowdowns on particularly old machines.

- Suggested [uBlock](https://github.com/gorhill/uBlock/) rules to stop most fonts from being downloaded in the first place (untick the general option to block all fonts):

```
* fast.fonts.net * block
* fonts.googleapi.com * block
* fonts.googleapis.com * block
* fonts.gstatic.com * block
* use.typekit.net * block
```

- Monospace detection needs more work (e.g. for code editors).

- Personal choice of fonts for optimal legibility (browser settings):
	- Proportional: `Sans Serif`
		- Size: `16`
	- Serif: [VenturisSans ADF Ex](https://arkandis.tuxfamily.org/tugfonts.htm)
	- Sans-serif: [Alegreya Sans](https://www.huertatipografica.com/en/fonts/alegreya-sans-ht)
	- Monospace: [PT Mono](https://company.paratype.com/pt-sans-pt-serif)
		- Size: `14`
	- Minimum font size: `12`
